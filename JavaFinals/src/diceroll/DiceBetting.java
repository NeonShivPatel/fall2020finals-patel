package diceroll;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
public class DiceBetting implements EventHandler<ActionEvent>{
	private DiceGame game;
	private int amountBet;
	private TextField resultField;
	private TextField balanceField;
	private TextField previousChoice;
	private String choice;
	
	
	public DiceBetting (TextField resultField, TextField balanceField, TextField inputField, TextField previousChoice, DiceGame game) {
		//Added a try catch in case the player did not input a number as a bet
		try {
		this.resultField = resultField;
		this.balanceField = balanceField;
		this.previousChoice = previousChoice;
		this.choice = previousChoice.getText();
		this.game = game;
		this.amountBet = Integer.valueOf(inputField.getText());
		}
		catch(NumberFormatException e) {
			this.amountBet = -1;
		}
	}
	
	//Method handle will play the game based on the choice made by user and then edit the GUI depdning on the outcome
		public void handle(ActionEvent e) {
			//If statement in case player did not input a number a bet
			if (this.amountBet == -1) {
				this.resultField.setText("You have made a mistake while inputting the bet. Make sure to input a whole number.");
				previousChoice.setText("Previous Choice: Error");
			}
			else {
			String results = this.game.playDice(this.amountBet, this.choice);
			this.balanceField.setText("Balance: " + game.getBalance());
			this.resultField.setText(results);
			}
		}
}

