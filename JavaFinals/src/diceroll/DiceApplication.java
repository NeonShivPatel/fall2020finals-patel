package diceroll;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class DiceApplication extends Application{
	private DiceGame game = new DiceGame();
	
	@Override
	public void start(Stage stage) throws Exception {
		Group root = new Group(); 
		
		//Creating the HBox and it's TextFields
		HBox firstRow = new HBox();
		TextField inputField = new TextField("Enter your bettings here");
		TextField previousChoice = new TextField("Previous Choice: ");
		
		HBox buttons = new HBox();
		Button smallButton = new Button("Small");
		Button largeButton = new Button("Large");
		
		
		HBox displayFields = new HBox();
		TextField resultField = new TextField("Results will be displayed here");
		TextField balanceField = new TextField("Balance: 500");
		
		//Adding events to each of the button 
		smallButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				previousChoice.setText("Previous Choice: Small");
				DiceBetting newGame = new DiceBetting(resultField, balanceField, inputField, previousChoice, game);
				newGame.handle(e);
				
			}
		});
		
		largeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				previousChoice.setText("Previous Choice: Large");
				DiceBetting newGame = new DiceBetting(resultField, balanceField, inputField, previousChoice, game);
				newGame.handle(e);
			}
		});
		
		
		//Setting the width of TextFields
		inputField.setPrefWidth(200);
		resultField.setPrefWidth(600);
		
		//Adding the TextFields to it's respective HBox
		firstRow.getChildren().addAll(inputField, previousChoice);
		buttons.getChildren().addAll(smallButton, largeButton);
		displayFields.getChildren().addAll(resultField, balanceField);

		
		//Creating the VBox and adding all other fields to it
		VBox vbox = new VBox();
		vbox.getChildren().addAll(firstRow, buttons, displayFields);
		
		//Adding the VBox to the root
		root.getChildren().add(vbox);
		
		//Giving the scene it's attributes
		Scene scene = new Scene(root, 800, 400); 
		scene.setFill(Color.BLACK);

		//Adding the scene to the stage
		stage.setTitle("Dice Betting Show"); 
		stage.setScene(scene); 
		
		stage.show(); 
		
	}

	public static void main (String[] args) {
		Application.launch(args);
	}
}
