package diceroll;

import java.util.Random;

public class DiceGame {
	private int balance = 500;
	private int[] dice = new int[] {1,2,3,4,5,6};
	public DiceGame() {};
	
	public int getBalance() {
		return this.balance;
	}
	
	
	//Method playDice will basically roll a Die, let the user know if he one or lost and add/remove money from his balance
	public String playDice(int amountBet, String choice) {
		if (amountBet > balance) {
			return "You cannot bet this round because you don't have enough money";
		}
		
		String message = "";
		Random randgen = new Random();
		int number = randgen.nextInt(6);
		
		String result = rollDie(number);
		
		//if statement that checks if he guessed correctly
		if (choice.equalsIgnoreCase(result)) {
			message = "The result was " + dice[number] + ". Congratulations, you guessed correctly!		:)";
			balance = balance + amountBet;
		}
		else {
			message = "The result was " + dice[number] + ". Sadly you guessed incorrectly.		:(";
			balance = balance - amountBet;
		}
		
		return message;
	}
	
	//The rollDie method will roll a die and return whether the number that was rolled was small or large
	public String rollDie(int number) {
		String result = "";
		if (dice[number] <= 3) {
			result = "Small";
		}
		else if (dice[number] >= 4) {
			result = "Large";
		}
		return result;
	}
	
}
