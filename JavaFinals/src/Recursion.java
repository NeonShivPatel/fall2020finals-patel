
public class Recursion {
	private static int count = 0;
	
	public static void main(String[] args) {
		int[] numbers = new int[] {11,11,16,18,18};
		//n is the index of the final position in the array
		int n = numbers.length-1;
		System.out.println(recursiveCount(numbers, n));

	}
	
	//recursiveCount will loop through an array of numbers and count how many match the specific conditions
	public static int recursiveCount(int[] numbers, int n) {
		//If the number corresponds to the condition then we add to count
		if (numbers[n] < 10 && n%2 != 0 && numbers[n] > n) {
			count++;
		}
		//If n is equal 0 then we stop the method by returning count
		if (n == 0) {
			return count;
		}
		recursiveCount(numbers, n-1);
		
		return count;
	}
	
}
